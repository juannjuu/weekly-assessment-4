- How the workflow for git until it's updated to repository ? 
Ans : If we have not clone a project from a repository, we have to run (git init) command in git bash or cmd from 
the spesific folder that will be pushed into online repo. Run (git add .) command to add all folder into staged condition.
After that we should run (git commit) with a description that define the process of commit. If we have not push our project to 
the online repository yet, run (git add remote "repo-link") to connect the git from our local to the online repository.
Finally, run (git push) to upload commited file to repository.

- Whats git? What's git use for?
Ans : git is a popular version control system that records the changes of files to a dynamic repository. 
Also we can check our history of what has been made or changed, who and why. In git we can track our project history dan work together 
with others that allow to changes by multiple people to merged into one source. We can build a project to several version.
If we meet something up we can back to previous track we have made, so we can easily manage the project. Git is a software that runs 
in local computer. We can use online repository such as GitHub or GitLab to store a copy of the files.

- How to track git progress ?
Ans : We can track our progress of git in online hosts applications such as GitHub or GitLab that stored our files and their history.
Or we can check our log with using (git log) command in our bash.
